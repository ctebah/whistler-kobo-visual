<?php
/**
 * Definicije parametara svih SQL konekcija koje su nam na raspolaganju.
 * Na kraju proglasavamo jednu za default (nju koristi sajt po defaultu), 
 *  
 */
return (function () {
    if (APPLICATION_ENV == 'development') {
        // windows developer
        $___dbconnection_list = array(
            'local' => array(
                'ident' => 'local',
                'host' => '127.0.0.1',
                'user' => 'kobo',
                'pass' => 'kobo',
                'dbname' => 'kobotoolbox',
                'newlink' => false,
                'collation' => 'utf8',
                'port' => '15434'
            )
        );
    } else {
        $___dbconnection_list = array(
            'local' => array(
                'ident' => 'local',
                //'host' => '172.17.0.2',
                'host' => 'postgres',
                'user' => 'kobo',
                'pass' => 'kobo',
                'dbname' => 'kobotoolbox',
                'newlink' => false,
                'collation' => 'utf8',
                'collation' => '5432'
            )
        );
    }
    
    // definisemo default konekciju - ovde menjamo default mysql server!
    $___dbconnection_list['default'] = & $___dbconnection_list['local'];
    // konekcija ka master serveru gde radimo sve updejte!
    $___dbconnection_list['master_server'] = & $___dbconnection_list['local'];
    
    return $___dbconnection_list;
})();
