<?php
namespace Middleware;

use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class AccessRestriction extends \Ufw\Middleware\Base
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->getController()->accessRestriction();
        return $handler->handle($request);
    }
}